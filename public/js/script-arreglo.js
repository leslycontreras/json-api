var datos = [
{
 nombre: 'Saul',
 apellido: 'Salazar',
 edad: 23,
 municipio: 'Guadalupe'
},
{
 nombre: 'Lesly',
 apellido: 'Contreras',
 edad: 17,
 municipio: 'Santa Catarina'
},
{
 nombre: 'Julio',
 apellido: 'Guzman',
 edad: 23,
 municipio: 'San Nicolas'
},
{
 nombre: 'Joaquin',
 apellido: 'Reyes',
 edad: 17,
 municipio: 'Santa Catarina'
},
{
 nombre: 'Enrique',
 apellido: 'Galvan',
 edad: 17,
 municipio: 'Santa Catarina'
},
{
 nombre: 'Kelvin',
 apellido: 'Rangel',
 edad: 18,
 municipio: 'Santa Catarina'
},
{
 nombre: 'Eduardo',
 apellido: 'Fraustro',
 edad: 17,
 municipio: 'Santa Catarina'
},
{
 nombre: 'Diego',
 apellido: 'Ambriz',
 edad: 20,
 municipio: 'Apodaca'
},
{
 nombre: 'Christian',
 apellido: 'Vega',
 edad: 17,
 municipio: 'Santa Catarina'
}
];

  var tabla = document.getElementById('tabla-datos')
  for(var i=0; i<datos.length; i++){
    var persona = datos[i];
    var etr = document.createElement('tr');
    tabla.appendChild(etr);

    var nombres = document.createElement('td');
    nombres.innerHTML = persona.nombre;
    etr.appendChild(nombres);

    var apellidos = document.createElement('td');
    apellidos.innerHTML = datos[i].apellido;
    etr.appendChild(apellidos);

    var edades = document.createElement('td');
    edades.innerHTML = datos[i].edad;
    etr.appendChild(edades);

    var municipios = document.createElement('td');
    municipios.innerHTML = datos[i].municipio;
    etr.appendChild(municipios);
  }
$('table').DataTable();
